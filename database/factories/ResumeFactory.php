<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Resume>
 */
class ResumeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(),
            'time' => $this->faker->word(),
            'desc' => $this->faker->paragraph(1),
            'place' => $this->faker->word(),
            'skils' => $this->faker->word(),
            'category' => $this->faker->word(3),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
