<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\portofolio>
 */
class PortofolioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(),
            'image' => $this->faker->mimeType(),
            'typeId' => $this->faker->numberBetween(1, 2),
            'desc' => $this->faker->paragraph(1),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
