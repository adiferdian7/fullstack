@extends('admin.tamplate.master')
@section('content')
@section('portofolio', 'active')

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
        </div>
        
        <!-- Content Row -->
        <div class="card shadow mb-4" id="sapi">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Detil User</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Type</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Type</th>
                                <th>Description</th>
                            </tr>
                        </tfoot>
                        @foreach($portofolio as $porto)
                        <tbody>
                            <tr>
                                <td>{{$porto->title}}</td>
                                <td>{{$porto->image}}</td>
                                <td>{{$porto->typeId}}</td>
                                <td>{{$porto->desc}}</td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection