<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Type;
use App\Models\Portofolio;

use function PHPSTORM_META\type;

class IndexController
{
    public function index(){
        $users = User::all()->sortBy('name');
        $type = Type::all();
        $porto = Portofolio::all();
        $data = Type::Join('portofolios', 'portofolios.typeId', '=', 'types.typeId')->where('portofolios.typeId', '=', 2)->get();
        // dd($data);
        return view('admin.index', ['users' => $users, 'data' => $data]);
    }

    public function send(Request $request){
        $data = new User();
        $data->name = $request->nama;
        $data->email = $request->email;
        $data->save();
    }
}
