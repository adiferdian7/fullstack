<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Detil;

class DetilController
{
    public function index(){
        $detil = Detil::all()->sortBy('name');
        return view('admin.detil', ['detil' => $detil]);
    }
}
