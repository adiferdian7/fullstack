<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Portofolio;

class PortofolioController
{
    public function index(){
        $portofolio = Portofolio::all();
        return view('admin.portofolio', ['portofolio' => $portofolio]);
    }
}
